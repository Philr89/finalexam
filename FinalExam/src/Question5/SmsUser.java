package Question5;

public class SmsUser extends User {

	private String phoneNumber;

	public SmsUser(String firstName, String lastName, Address address, String phoneNumber) {
		super(firstName, lastName, address);
		this.phoneNumber = phoneNumber;

		if (phoneNumber.matches("[0-9]+") == false && phoneNumber.length() > 7) {
			throw new IllegalArgumentException("Phone number must only contain digits");
		}

	}

}
