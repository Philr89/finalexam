package Question5;

import java.io.FileWriter;
import java.io.IOException;

public class SendEmail implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) throws IllegalArgumentException {
		if (body.contains("@") && body.contains(".") && !body.isEmpty() && !body.contains("^") && !body.contains("*")
				&& !body.contains("!"))
		// Messy but will have to do
		{
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void sendMessage(Message message) {
		try {
			FileWriter fileWriter = new FileWriter("email.txt");
			fileWriter.write(message.toString());
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
