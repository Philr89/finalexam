package Question5;

import java.io.FileWriter;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SendSms implements ISendInfo {

	@Override
	public boolean validateMessage(User sender, User receiver, String body) throws IllegalArgumentException {
		Pattern pattern = Pattern.compile(body);
		Matcher matcher = pattern.matcher("0-9");
		boolean matchFound = matcher.find();
		// Checks that the body contains only digits and is 10 characters long
		if (matchFound && body.length() == 10 && body.length() < 160 && !body.isEmpty() && !body.contains("&#@")) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public void sendMessage(Message message) {
		try {
			FileWriter fileWriter = new FileWriter("sms.txt");
			fileWriter.write(message.toString());
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
